#! /bin/bash
cp /opt/docker/daytime /etc/xinetd.d/daytime
cp /opt/docker/echo /etc/xinetd.d/echo
cp /opt/docker/chargen /etc/xinetd.d/chargen

service xinetd start
