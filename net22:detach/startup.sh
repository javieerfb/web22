#!/bin/bash
cp /opt/docker/daytime /etc/xinetd.d/
cp /opt/docker/echo /etc/xinetd.d/
cp /opt/docker/chargen /etc/xinetd.d/

service xinetd start
xinetd -dontfork	#Básicamente le dice que se ejecute en foreground
/bin/bash
